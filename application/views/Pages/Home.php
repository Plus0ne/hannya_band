<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>
		Welcome | Hannya Band
	</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<link rel="stylesheet" href="<?=base_url()?>assets/web-fonts-with-css/css/fontawesome-all.min.css" integrity="sha384-G0fIWCsCzJIMAVNQPfjH08cyYaUtMwjJwqiRKxxE/rx96Uroj1BtIQ6MLJuheaO9" crossorigin="anonymous">
	<style type="text/css">
		html
		{
			overflow-x: hidden;

		}

		body
		{

			background-image: url('<?=base_url()?>/assets/images/24058766_1733175750034196_329309052379721949_n.jpg');
			background-repeat: no-repeat;
			background-position: top;
			background-attachment: fixed;
			background-size: cover;
		}
		.navbar-back
		{

			
			background-color: black;
			width: 100%;
			background: rgb(0, 0, 0, 0.5)
		}
		.ul-navbar
		{
			
		}
		.btn-a-design
		{
			padding: 20px 30px 20px 30px !important;
			border-radius: 0px !important;
			color: white;
			font-size: 19px;
		}
		.btn-a-design:hover
		{

			background: rgb(255, 255, 255, 0.5) !important;
			color: white;
		}
		.body-design
		{

		}
	</style>
</head>
<body class="body-design">
<div class="row">
	<div class="col-lg-12">
		<nav class="navbar navbar-static-top navbar-back">
			<ul class="nav nav-pills ul-navbar" style="float: right !important;">
			   	<li role="presentation" class=""><a class="btn-a-design" href="#">
			   	<i style="color: white;" class="fa fa-chevron-right "> </i> Home</a></li>
		    	<li role="presentation" class=""><a class="btn-a-design" href="#">
		    	<i style="color: white;" class="fa fa-chevron-right "> </i> Pictures</a></li>
		    	<li role="presentation" class=""><a class="btn-a-design" href="#">
		    	<i style="color: white;" class="fa fa-chevron-right "> </i> Videos</a></li>
		    	<li role="presentation" class=""><a class="btn-a-design" href="#">
		    	<i style="color: white;" class="fa fa-chevron-right "> </i> About</a></li>
		    	<li role="presentation" class=""><a class="btn-a-design" href="#" type="button" data-toggle="modal" data-target="#exampleModal">
		    		<i style="color: white;" class="fa fa-chevron-right "> </i> Like us</a></li>
		    </ul>
		</nav>
	</div>
</div>

<div class="row">
	<div class="col-lg-12 col-lg-offset-1">
		<div class="container">
			<div>

			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Like us on Facebook <i style="color: blue;" class="fab fa-facebook"></i></h3>
      </div>
      <div class="modal-body">
        <div style="text-align: center;">
        	<div class="fb-page" data-href="https://www.facebook.com/hannyabandPH/" data-tabs="timeline" data-width="400" data-height="70" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/hannyabandPH/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/hannyabandPH/">Hannya</a></blockquote>
        </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<footer style="">
	<!-- Footer here -->
</footer>
<div id="fb-root"></div>
<script>
	(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0';
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
</script>

</body>
	<script type="text/javascript" src="<?=base_url()?>assets/js/jquery-3.3.1.min.js"></script>
	<!-- Latest compiled and minified JavaScript -->
	<script src="<?=base_url()?>assets/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	
</html>